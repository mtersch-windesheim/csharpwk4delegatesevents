﻿using System;
using static EventDemo.Model;

namespace EventDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Model model = new Model();
            model.AddObserver(mijnEventHandler);
            model.ZetGetal(42);
        }
        public static void mijnEventHandler(object model, ModelEventArgs modelEventArgs)
        {
            if (model is Model)
            {
                Console.WriteLine($"Nieuwe waarde van getal in model: {((Model)model).GetGetal()}");
                Console.WriteLine($"Waarde in ModelEventArgs: {modelEventArgs.Getal}");
            }
        }
    }
}
