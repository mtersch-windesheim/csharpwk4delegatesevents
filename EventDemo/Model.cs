﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventDemo
{
    class Model
    {
        public delegate void Observer(object model, ModelEventArgs modelEventArgs);
        
        private int _getal;

        private event Observer _observer;

        public void AddObserver(Observer observer)
        {
            _observer += observer;
        }
        public int GetGetal() { return _getal; }
        public void ZetGetal(int getal)
        {
            _getal = getal;
            _observer(this, new ModelEventArgs() { Getal = getal });
        }
    }
}
