﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdressingTheUnitedNations
{
    public class DonaldTrump
    {
        // Donald Trump's 'Shout' static method matches the signature in delegate type 'SpeakForTheGeneralAssembly' (return type 'void', parameter of type 'Languages[]')
        public static void Shout(Languages[] whatever)
        {
            // Forget about different languages, I'll speak English anyway!
            Console.WriteLine("AMERICA IS GREAT! AND I'M EVEN GREATER!");
        }
    }
}
