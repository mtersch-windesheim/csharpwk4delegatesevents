﻿using System;

namespace AdressingTheUnitedNations
{
    class Program
    {
        static void Main(string[] args)
        {
            GeneralAssembly generalAssembly = new GeneralAssembly();
            generalAssembly.HoldMeetingOfGeneralAssembly();
        }
    }
}
