﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdressingTheUnitedNations
{
    public class GretaThunberg
    {
        // Greta Thunberg's 'ShoutHowDareYou' method matches the signature in delegate type 'SpeakForTheGeneralAssembly' (return type 'void', parameter of type 'Languages[]')
        public void ShoutHowDareYou(Languages[] doISpeakThese)
        {
            if (doISpeakThese.Contains(Languages.Swedish))
            {
                // Yes!!! I get to speak Swedish today!
                Console.WriteLine("Hur vågar du!...");
            } else
            {
                // Let's hope the English interpreter came to work today...
                Console.WriteLine("How dare you! ...");
            }
        }
        public string HowDareYou()
        {
            return "How dare you!...";
        }
    }
}
