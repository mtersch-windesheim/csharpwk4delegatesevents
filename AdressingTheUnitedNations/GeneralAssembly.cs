﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdressingTheUnitedNations
{
    public class GeneralAssembly
    {
        // In the real world, delegates are people who speak on behalf of some country, organisation or group which has a certain interest.
        // At a meeting, the delegates who showed up speak on behalf of their own country/organisation/group.

        // In C# delegates are the values in a variable that hold references to methods which have a certain signature.
        // When executing a delegate-typed variable, the methods that are referenced in the variable run code that is part of their own class.

        // Step 1: define some delegate type so you can't reference a method with one signature from a variable with a different signature.

        // Delegate type for any speech to be held before the General Assembly - speakers must consider the languages that can be translated.
        public delegate void SpeakForTheGeneralAssembly(Languages[] translationsAvailable);

        // And a delegate type with a return value - let's see how multicast delegates behave with return values!
        public delegate string GetSpeechText();

        // Step 2: declare a variable of this delegate type to hold references to methods with this signature.

        // Variable (of delegate type) to register the speeches that will be held
        public SpeakForTheGeneralAssembly Speeches { get; set; }
        public GetSpeechText SpeechText { get; set; }

        public void HoldMeetingOfGeneralAssembly()
        {
            // Step 3: put references to methods in the variable.
            // (NB: we're using '+=' to put *multiple* references in *one* variable - this is called a 'multicast delegate')

            // What speeches will we hear today? Add each speech-method with '+='
            Speeches += new MarkRutte().MumbleAboutTrade;
            Speeches += new GretaThunberg().ShoutHowDareYou;
            Speeches += DonaldTrump.Shout;                  // It doesn't matter if the speech-method is static or non-static

            String dateString = DateTime.Now.ToString();
            dateString = dateString.Substring(0, dateString.IndexOf(' '));
            OpenSession(dateString);
            // Check if anyone registered to hold a speech

            // Speeches?.Invoke(interpretersOnDutyToday);      // Andere notatie voor 'if (Speeches != null) { Speeches(...); }

            if (Speeches != null)
                
                // Step 4: call the variable (of delegate type) just like a regular method - this will run all referenced methods
                // (with the same parameters you used when calling the delegate variable).

                // If so, start the registered speeches
                Speeches(interpretersOnDutyToday);
            else
                Console.WriteLine("Whow! No speeches today?!");
            CloseSession();

            // Greta on repeat
            SpeechText += new GretaThunberg().HowDareYou;
            // Mark Rutte can do that too
            SpeechText += new MarkRutte().EconomicsAgain;
            // Do the last-minute speeches
            Console.WriteLine(SpeechText());
            // Notice how this will only show the *last* return value!
        }

        private Languages[] interpretersOnDutyToday = new Languages[] { Languages.English, Languages.Dutch };
        private void OpenSession(string date)
        {
            Console.WriteLine($"blahblah...Welcome...blah...session of {date}...blah");
        }
        private void CloseSession()
        {
            Console.WriteLine("Alright, time's up! Let's get some drinks!");
        }
    }
    public enum Languages
    {
        English,
        French,
        Chinese,
        Russian,
        Spanish,
        Arabic,
        German,
        Dutch,
        Swedish
    }
}
