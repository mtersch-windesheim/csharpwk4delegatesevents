﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdressingTheUnitedNations
{
    public class MarkRutte
    {
        // Mark Rutte's 'MumbleAboutTrade' method matches the signature in delegate type 'SpeakForTheGeneralAssembly' (return type 'void', parameter of type 'Languages[]')
        public void MumbleAboutTrade(Languages[] pickOne)
        {
            // Check out how many languages I speak!
            if (pickOne.Contains(Languages.Dutch))
            {
                Console.WriteLine("blabla...economie...blabla...belangrijk");
            } else if (pickOne.Contains(Languages.French))
            {
                Console.WriteLine("blabla...economie...blabla...tres important");
            }
            else if(pickOne.Contains(Languages.German))
            {
                Console.WriteLine("blabla...Wirtschaft...blabla...wichtig");
            }
            else
            {
                Console.WriteLine("blabla...economy...blabla...important");
            }
        }
        public string EconomicsAgain()
        {
            return "But the economy is REALLY important!";
        }
    }
}
