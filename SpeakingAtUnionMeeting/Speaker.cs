﻿namespace SpeakingAtUnionMeeting
{
    public interface ISpeaker
    {
        public void Speak(string subject);
    }
}