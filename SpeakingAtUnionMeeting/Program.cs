﻿using System;

namespace SpeakingAtUnionMeeting
{
    class Program
    {
        static void Main(string[] args)
        {
            UnionMeeting meeting = new UnionMeeting();
            meeting.Convene();
        }
    }
}
