﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpeakingAtUnionMeeting
{
    public class Secretary : ISpeaker
    {
        public void Speak(string subject)
        {
            Console.WriteLine($"We secretaries have a strong opinion about {subject}");
        }
    }
}
