﻿using System;
using System.Collections.Generic;

namespace SpeakingAtUnionMeeting
{
    public class UnionMeeting
    {
        private List<ISpeaker> speakers;
        public UnionMeeting()
        {
            speakers = new List<ISpeaker>();
        }

        public void Convene()
        {
            string subject = "working hours";
            speakers.Add(new Secretary());
            speakers.Add(new CEO());
            // speakers.Add(new ConcernedNeighbour());  // Hmmm... ConcernedNeighbour can't speak at the meeting, as he doesn't implement ISpeaker.
            // P.S.: ConcernedNeighbour *does* have a method with return type 'void' and one parameter of type 'string' -> would work with a delegate!

            Console.WriteLine($"I declare today's union meeting about {subject} opened, and give the floor to our speakers.");
            foreach (ISpeaker speaker in speakers)
            {
                speaker.Speak(subject);
            }
            Console.WriteLine($"I thank all speakers for their thoughts on the subject of {subject}, and call this meeting to a close!");
        }
    }
}