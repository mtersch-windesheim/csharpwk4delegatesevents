﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpeakingAtUnionMeeting
{
    public class CEO : ISpeaker
    {
        public void Speak(string subject)
        {
            Console.WriteLine($"As a CEO I think my staff shouldn't complain about {subject}");
        }
    }
}
