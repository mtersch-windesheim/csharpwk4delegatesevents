﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpeakingAtUnionMeeting
{
    public class ConcernedNeighbour
    {
        public void SayAWordAbout(string subject)
        {
            Console.WriteLine($"As a neighbour of the factory I have some concerns about {subject} as well.");
        }
    }
}
