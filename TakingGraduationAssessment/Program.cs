﻿using System;

namespace TakingGraduationAssessment
{
    class Program
    {
        static void Main(string[] args)
        {
            GraduationAssessment assessment = new GraduationAssessment();
            SchoolSupervisor schoolSupervisor = new SchoolSupervisor();
            ExternalSupervisor externalSupervisor = new ExternalSupervisor();

            assessment.QuestionTime += schoolSupervisor.AskTheoreticalQuestion;
            assessment.QuestionTime += externalSupervisor.CheckPracticalUsefulness;
            assessment.QuestionTime += NastyQuestion;

            // Hmmm... theory only please, no questions on practicality!
            assessment.QuestionTime -= externalSupervisor.CheckPracticalUsefulness;

            assessment.GraduateName ="Henkie";
            assessment.Proceed("master");
        }
        static void NastyQuestion(object ceremony, GraduationEventArgs gea)
        {
            if (!(ceremony is GraduationAssessment))
            {
                Console.WriteLine("Whoops, I'm in the wrong place!");
            } else
            {
                GraduationAssessment assessment = (GraduationAssessment)ceremony;
                Console.WriteLine($"Dear {assessment.GraduateName}, do you think you can become a {gea.Degree} with this kind of work?");
            }
        }
    }
}
