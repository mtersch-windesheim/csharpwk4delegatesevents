﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TakingGraduationAssessment
{
    class GraduationEventArgs : EventArgs
    {
        public string Degree { get; set; }
        public GraduationEventArgs(string degree)
        {
            Degree = degree;
        }
    }
}
