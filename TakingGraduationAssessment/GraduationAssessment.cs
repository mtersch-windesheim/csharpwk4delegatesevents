﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TakingGraduationAssessment
{
    class GraduationAssessment
    {
        private QuestionTimeHasCome _questionTime;

        public delegate void QuestionTimeHasCome(object assessment, GraduationEventArgs eventArgs);

        public event QuestionTimeHasCome QuestionTime {
            add {
                _questionTime += value;
            } remove {
                _questionTime -= value;
            }
        }
        public string GraduateName { get; set; }
        public void Proceed(string degree)
        {
            Console.WriteLine($"The graduation assessment for {GraduateName} to get the degree of {degree} has started.");
            _questionTime?.Invoke(this, new GraduationEventArgs(degree));
            Console.WriteLine($"Congratulations {GraduateName}, you have earned the degree of {degree}!");
        }
    }
}
