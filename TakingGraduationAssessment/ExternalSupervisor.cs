﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TakingGraduationAssessment
{
    class ExternalSupervisor
    {
        public void CheckPracticalUsefulness(object ceremony, GraduationEventArgs args)
        {
            if (ceremony is GraduationAssessment)
            {
                GraduationAssessment meeting = (GraduationAssessment)ceremony;
                Console.WriteLine($"Hi {meeting.GraduateName}, how useful is your work in daily life?");
            }
        }
    }
}
