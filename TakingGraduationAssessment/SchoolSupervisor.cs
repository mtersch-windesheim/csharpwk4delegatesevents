﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TakingGraduationAssessment
{
    class SchoolSupervisor
    {
        public void AskTheoreticalQuestion(object assessment, GraduationEventArgs gea)
        {
            if (assessment is GraduationAssessment)
            {
                GraduationAssessment ga = (GraduationAssessment)assessment;
                Console.WriteLine($"Hello {ga.GraduateName}, as a future {gea.Degree} you should know all about invariants; please tell me how you used those in your project.");
            } else
            {
                Console.WriteLine("Hmmm... I thought this would be a graduation assessment.");
            }
        }
    }
}
